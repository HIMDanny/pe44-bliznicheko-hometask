/**
 * # HW6
 * ## Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
 *
 * ## Технічні вимоги:
 * - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
 * 	1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
 * 	2. Створити метод getAge() який повертатиме скільки користувачеві років.
 * 	3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
 * - Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
 *
 */

"use strict";

let createNewUser = () => {
	let firstName;
	let lastName;
	let birthday;

	const newUser = {
		firstName,
		lastName,
		birthday,

		getLogin() {
			return (this.firstName[0] + this.lastName).toLowerCase();
		},

		setFirstName(firstName) {
			Object.defineProperty(this, 'firstName', { value: firstName });
		},

		setLastName(lastName) {
			Object.defineProperty(this, 'lastName', { value: lastName });
		},

		convertToDateFormat(str) {
			const year = str.slice(6);
			const month = str.slice(3, 5) - 1;
			const day = str.slice(0, 2);
			const newDate = new Date(year, month, day);
			return newDate.getTime();
		},

		getAge() {
			const today = new Date();
			const BIRTHDAY = new Date(this.convertToDateFormat(this.birthday));
			const userAge = today.getFullYear() - BIRTHDAY.getFullYear();

			// Если день рождения в этом году ещё не наступил
			if ((BIRTHDAY.getDate() > today.getDate()) && (BIRTHDAY.getMonth() > today.getMonth())) return userAge - 1;

			return userAge;
		},

		getPassword() {
			const BIRTHDAY = new Date(this.convertToDateFormat(this.birthday));
			return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + BIRTHDAY.getFullYear());
		},
	}

	Object.defineProperties(newUser, {
		firstName: {
			value: prompt("Enter your name"),
			writable: false,
			configurable: true,
		},
		lastName: {
			value: prompt("Enter your last name"),
			writable: false,
			configurable: true,
		},
		birthday: {
			value: prompt("Enter your birthday\n(Format: dd.mm.yyyy)"),
		}
	})

	return newUser;
}

const user = createNewUser();

console.log(user);
console.log(`User login: ${user.getLogin()}`);
console.log(`User age: ${user.getAge()}`);
console.log(`User password: ${user.getPassword()}`);