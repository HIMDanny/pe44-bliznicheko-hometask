/**
 * # HW 2
 * 
 * ## Необязательное задание продвинутой сложности:
 * - Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
 * - Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.
 */

"use strict";

let firstNumber, secondNumber;

do {
	firstNumber = +prompt("Введите первое натуральное число");
	secondNumber = +prompt("Введите второе натуральное число");

	if (firstNumber > secondNumber) {
		alert("Первое число должно быть меньше второго");
	}
}
while (firstNumber > secondNumber || !Number.isInteger(firstNumber) || !Number.isInteger(secondNumber))

nextNumber:
	for (let i = firstNumber; i <= secondNumber; i++) {
		if (i === 1) continue nextNumber;
		for (let j = 2; j < i; j++) {
			if (!(i % j)) continue nextNumber;
		}
		console.log(i);
	}