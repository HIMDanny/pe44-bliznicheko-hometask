# Ответы на вопросы

## Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
- **var** переменная видна везде, а так же они существуют до объявления, в отличии от **let** и **const**.
- **let** видно только внутри определённого блока, и поэтому полезна в использовании с циклами. Нельзя переобъявлять.
- **const** является константой. Нельзя переопределять. В остальном то же самое, что и **let**.

## Почему объявлять переменную через var считается плохим тоном?
Так как **var** переменная видна везде, это вызывает некоторые проблемы в последующем. В циклах придётся всегда создавать разные переменные (в отличии от **let**, где в каждом цикле можно использовать одинаковое название, например ***i***). Так как **var** переменные существуют и до их объявления, это может вызвать некие путаницы в коде, особенно если он большой.