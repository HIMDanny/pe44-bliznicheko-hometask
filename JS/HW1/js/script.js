/**
 * # HW1
 *
 * ## Завдання:
 * 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
 * 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
 * 3. Запитайте у користувача якесь значення і виведіть його в консоль.
 */

"use strict";

const name = "Daniel";
const admin = name;

const days = 5;

const userAnswer = prompt("Enter your favourite colour");

console.log(admin);

console.log(`${days}s`);

console.log(userAnswer);