/**
 * # HW13
 * Реалізувати програму, яка циклічно показує різні картинки.
 */

"use strict";
const btnWrapper = document.querySelector('.controll-btns');
const stopBtn = btnWrapper.querySelector('.btn--stop');
const resumeBtn = btnWrapper.querySelector('.btn--resume');

const changeDisabledBtn = () => {
	if (stopBtn.disabled === false) {
		resumeBtn.disabled = false;
		stopBtn.disabled = true;
	}
	else {
		resumeBtn.disabled = true;
		stopBtn.disabled = false;
	}
}

let currentImgIndex = 1;
const showImage = () => {
	btnWrapper.style.display = "flex";
	const imageCollection = document.querySelectorAll('.image-to-show');
	imageCollection.forEach(img => {
		if (img.classList.contains('image--active')) {
			img.classList.remove('image--active');
		}
	});

	currentImgIndex++;
	if (currentImgIndex > imageCollection.length) { currentImgIndex = 1 };
	imageCollection[currentImgIndex - 1].classList.add('image--active');


}
let slideShow = setInterval(showImage, 3000);

stopBtn.addEventListener('click', () => {
	clearInterval(slideShow);
	changeDisabledBtn();
});

resumeBtn.addEventListener('click', () => {
	slideShow = setInterval(showImage, 3000);
	changeDisabledBtn();
})