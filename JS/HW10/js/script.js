/**
 * # HW10
 * ## Реалізувати перемикання вкладок (таби) на чистому Javascript.
 * 
 * ## Технічні вимоги:
 * - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
 * - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
 * - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
 * 
 */

const showTabContent = () => {
	// const tabs = document.querySelector('.tabs');
	const tabs = document.querySelector('.tabs');
	const activeTab = tabs.querySelector('.active');
	const tabsContent = document.querySelectorAll('.tabs-content li');

	tabsContent.forEach(content => {
		if (activeTab.dataset.tabTitle === content.dataset.tabContent) {
			content.style.display = "unset";
		}
	})

	tabs.addEventListener('click', (e) => {
		e.currentTarget.childNodes.forEach(tab => {
			tab.classList?.remove('active');
		})
		e.target.classList.add('active');

		tabsContent.forEach(content => {
			content.style.display = "";
			if (e.target.dataset.tabTitle === content.dataset.tabContent) {
				content.style.display = "unset";
			}
		});
	});


}

showTabContent();