/**
 * #HW16
 * ##Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.
 *
 * ##Технические требования:
 * - Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
 * - Считать с помощью модального окна браузера число, которое введет пользователь (n).
 * - С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
 * - Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
 *
 * ##Необязательное задание продвинутой сложности:
 * - После ввода данных добавить проверку их корректности. Если пользователь не ввел число, либо при вводе указал не число, - спросить число заново (при этом значением по умолчанию для него должна быть введенная ранее информация).
 * */
"use strict";

let isNumber = (number) => {
	if (!number) return false;
	return true;
}

let askNumber = (message) => {
	let number;
	do {
		number = +prompt(message, number);
	}
	while (!isNumber(number))

	return number;
}

let calcFibonacci = (F1, F2, serial) => {
	if (serial === 1) {
		return F1;
	} else if (serial === 2) {
		return F2;
	} else if (serial > 0 && serial <= 3) {
		return F1 + F2;
	}
	return calcFibonacci(F1, F2, serial - 1) + calcFibonacci(F1, F2, serial - 2);
}

let firstNumber = askNumber("Enter first sequence number");
let secondNumber = askNumber("Enter second sequence number");;
let serialNumber = askNumber("Enter the serial number of the Fibonacci number to be found");;

console.log(calcFibonacci(firstNumber, secondNumber, serialNumber));