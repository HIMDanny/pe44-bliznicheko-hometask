/** 
 * #HW 3
 * Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
 * 
 * ## Технические требования:
 * - Считать с помощью модального окна браузера два числа.
 * - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
 * - Создать функцию, в которую передать два значения и операцию.
 * - Вывести в консоль результат выполнения функции.
 * 
 * ## Необязательное задание продвинутой сложности:
 * - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
 */

"use strict";

let firstNumber, secondNumber, operation;

do {
	firstNumber = +prompt("Введите первое число", firstNumber);
	secondNumber = +prompt("Введите второе число", secondNumber);
	operation = prompt("Введите одну из операций: +, -, *, /", operation);
	if (!(operation === "+") && !(operation === "-") && !(operation === "*") && !(operation === "/")) {
		alert("Неизвестный оператор :(");
	}
}
while (isNaN(firstNumber) || isNaN(secondNumber) || !(operation === "+") && !(operation === "-") && !(operation === "*") && !(operation === "/"))

function mathOperations(firstNumber, secondNumber, operation) {
	switch (operation) {
		case "+":
			return firstNumber + secondNumber;
		case "-":
			return firstNumber - secondNumber;
		case "*":
			return firstNumber * secondNumber;
		case "/":
			return firstNumber / secondNumber;
	}
}

alert(`${firstNumber} ${operation} ${secondNumber} = ${mathOperations(firstNumber, secondNumber, operation)}`)