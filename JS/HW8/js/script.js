/**
 * # HW8
 * 
 * ## Завдання:
 * 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000.
 * 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
 * 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.
 * 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
 * 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
 */

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000.
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(p => p.style.backgroundColor = "#ff0000");

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// const optionsList = document.getElementById('optionsList');
const optionsList = document.querySelector('#optionsList');
console.log(optionsList);

const optionsListParent = optionsList.parentElement;
console.log(optionsListParent);

if (optionsList.hasChildNodes()) {
	const optionsListChildren = optionsList.childNodes
	optionsListChildren.forEach(child => console.log(`Node name: ${child.nodeName}\nNode type: ${child.nodeType}`));
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.
// const testParagraph = document.querySelector('.testParagraph'); - null. Такого класу не існує. Але якщо замінити клас на id код буде працювати.
// testParagraph.innerText = "This is paragraph";
const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = "This is paragraph";

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeaderChildren = document.querySelector('.main-header').children;
console.log(mainHeaderChildren);

for (let i = 0; i < mainHeaderChildren.length; i++) {
	mainHeaderChildren[i].classList.add('nav-item');
}
console.log(mainHeaderChildren);

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitleCollection = document.querySelectorAll('.section-title');
console.log(sectionTitleCollection);
sectionTitleCollection.forEach(sectionTitle => sectionTitle.classList.remove('section-title'));
console.log(sectionTitleCollection);