/**
 * # HW7
 * ## Реалізувати функцію фільтру масиву за вказаним типом даних.
 *
 * ## Технічні вимоги:
 * - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
 * - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
 *
 */

"use strict";

const arr = [1, 4, "it is a string", true, "hello", false, { name: "John" }, undefined, null];

const filterBy = (arr, dataType) => {
	const newArray = arr.filter(elem => (typeof elem !== dataType));
	return newArray;
}

const allTypes = ["string", "number", "bigint", "boolean", "undefined", "object", "symbol"];

allTypes.forEach(type => console.log(filterBy(arr, type)));

console.log("----------------------------------------------------------------------------------------------------------------");

const anotherFilterBy = (arr, dataType) => {
	const newArray = [];
	arr.forEach(elem => {
		if (typeof elem !== dataType) {
			newArray.push(elem);
		}
	});
	return newArray;
}

allTypes.forEach(type => console.log(anotherFilterBy(arr, type)));