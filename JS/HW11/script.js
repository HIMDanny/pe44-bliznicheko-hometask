"use strict";

const inputWrapperCollection = document.querySelectorAll('.input-wrapper');
const passwordForm = document.querySelector('.password-form');

const handlePasswordInput = () => {
	inputWrapperCollection.forEach(item => {
		const password = item.querySelector('input');
		item.addEventListener('click', (e) => {
			if (e.target.tagName === "I") {
				if (e.target.classList.contains('fa-eye-slash')) {
					e.target.classList.remove('fa-eye-slash');
					e.target.classList.add('fa-eye');
					password.type = "password";
				}
				else {
					e.target.classList.remove('fa-eye');
					e.target.classList.add('fa-eye-slash');
					password.type = "text";
				}
			}
		});
	});
}

handlePasswordInput();

const compareAndValidatePasswords = () => {
	const wrongPasswordMessage = document.createElement('span');
	wrongPasswordMessage.className = "password-wrong";
	wrongPasswordMessage.innerText = "Потрібно ввести однакові значення";

	passwordForm.addEventListener('submit', (e) => {
		e.preventDefault();
		const password = e.currentTarget.querySelector('#password');
		const passwordSubmit = e.currentTarget.querySelector('#passwordSubmit');


		if (password.value && passwordSubmit.value && password.value === passwordSubmit.value) {
			wrongPasswordMessage.remove();
			alert('You are welcome');
		}
		else {
			passwordSubmit.after(wrongPasswordMessage);
		}
	});
}

compareAndValidatePasswords();