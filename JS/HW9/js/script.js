/**
 * # HW9
 * ## Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
 *
 * ## Технічні вимоги:
 * - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body).
 * - Кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
 *
 * ## Необов'язкове завдання підвищеної складності:
 * - Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
 * - Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
 */

"use strict";

/**
 *
 * @param {Array} arr - Array that will be added as a list on a page.
 * @param {HTMLElement} [parent=document.body] - DOM-element to which list will be added.
 */
const addListOnPage = (arr, parent = document.body) => {
	const list = `<ul>${arr.map((listItem) => `<li>${listItem}</li>`).join("")}</ul>`;
	parent.insertAdjacentHTML("afterbegin", list);
};

const parentElement = document.createElement("div");
document.body.append(parentElement);

const listArray1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const listArray2 = ["1", "2", "3", "sea", "user", 23];

addListOnPage(listArray1);
addListOnPage(listArray2, parentElement);