/**
 * # HW17
 * ## Створити об'єкт "студент" та проаналізувати його табель.
 *
 * ## Технічні вимоги:
 * - Створити порожній об'єкт student, з полями name та lastName.
 * - Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
 * - У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
 * - Порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
 * - Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.
 * 
 */
"use strict";

const student = {
	name: prompt("Enter your name"),
	lastName: prompt("Enter your last name"),
	tabel: {
		addSubject() {
			let subject;
			do {
				subject = prompt("Enter subject");
				if (subject)
					this[subject] = +prompt("Enter your grade");
			}
			while (subject)
			Object.defineProperty(student.tabel, "addSubject", { enumerable: false });
		},
	},
};

// const countBadGrades = () => {
// 	let badGrades = 0;
// 	for (let subjectGrade in student.tabel) {
// 		if (+student.tabel[subjectGrade] < 4) {
// 			badGrades++;
// 		}
// 	}
// 	return badGrades;
// }

// const countGPA = () => {
// 	let gradesSum = 0;
// 	let gradesAmount = 0;
// 	for (let subjectGrade in student.tabel) {
// 		gradesAmount++;
// 		gradesSum += +student.tabel[subjectGrade];
// 	}
// 	return +(gradesSum / gradesAmount).toFixed(2);
// }

const countBadGrades = () => {
	// Получаем значения свойств и создаём массив с этими значениями
	let badGradesValues = Object.values(student.tabel);
	let badGrades = 0;

	// Проверяем каждое значение в массиве: если значение меньше 4-х, то инкрементируем badGrades, иначе - ""
	badGradesValues.forEach(grades => grades < 4 ? badGrades++ : "");
	return badGrades;
}

const countGPA = () => {
	// Получаем значения свойств и создаём массив с этими значениями
	let grades = Object.values(student.tabel);

	// Суммруем все полученные значения
	let gradesSum = grades.reduce((previusValue, currentValue) => previusValue + currentValue);

	// Получаем колличество свойств
	let gradesAmount = Object.keys(student.tabel).length;

	// Находим среднее арифметическое и округляем до двух чисел после запятой
	return +(gradesSum / gradesAmount).toFixed(2);
}

student.tabel.addSubject();

console.log(`Середній бал студента ${student.name} - ${countGPA()}`)
if (countBadGrades() > 0) {
	console.log(`Кількість поганих оцінок - ${countBadGrades()}`);
	console.log("Студент не переведено на наступний курс.");
} else {
	console.log("Студент переведено на наступний курс.");

	if (countGPA() > 7)
		console.log("Студенту призначено стипендію.");
}