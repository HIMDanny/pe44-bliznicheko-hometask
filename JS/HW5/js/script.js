/**
 * # HW5
 * ## Реалізувати функцію створення об'єкта "юзер".
 *
 * ## Технічні вимоги:
 * - Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
 * - При виклику функція повинна запитати ім'я та прізвище.
 * - Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
 * - Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
 * - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
 *
 * ## Необов'язкове завдання підвищеної складності:
 * - Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
 */

"use strict";



let createNewUser = () => {
	let firstName;
	let lastName;

	const newUser = {
		firstName,
		lastName,

		getLogin() {
			return (this.firstName[0] + this.lastName).toLowerCase();
		},

		setFirstName(firstName) {
			Object.defineProperty(this, 'firstName', { value: firstName });
		},

		setLastName(lastName) {
			Object.defineProperty(this, 'lastName', { value: lastName });
		},
	}

	Object.defineProperties(newUser, {
		firstName: {
			value: prompt("Enter your name"),
			writable: false,
			configurable: true,
		},
		lastName: {
			value: prompt("Enter your last name"),
			writable: false,
			configurable: true,
		},
	})

	return newUser;
}

const user = createNewUser();


// user.firstName = "John"; // error
// user.lastName = "Doe"; // error
console.log(user);
console.log(user.getLogin());

user.setFirstName("Mark");
user.setLastName("Twen");
console.log(user);
console.log(user.getLogin());