"use strict";

const themes = [
	{
		title: 'main',
		href: 'css/themes/theme-main.css',
	},
	{
		title: 'magenta',
		href: 'css/themes/theme-magenta.css',
	}
];

const themeLink = document.querySelector('#theme');
const themeBtn = document.querySelector('.btn-theme');

let activeThemeTitle = localStorage.getItem('theme');
let activeThemeHref;

themes.forEach(theme => {
	if (localStorage.getItem('theme') == theme.title) {
		themeLink.setAttribute('href', theme.href);
		activeThemeTitle = theme.title;
	}
});

themeBtn.addEventListener('click', () => {
	if (activeThemeTitle === themes[1]['title']) {
		activeThemeTitle = themes[0]['title'];
		activeThemeHref = themes[0]['href'];
	}
	else {
		activeThemeTitle = themes[1]['title'];
		activeThemeHref = themes[1]['href'];
	}
	themeLink.setAttribute('href', activeThemeHref);
	localStorage.setItem('theme', activeThemeTitle);
});

