/**
 * # HW18
 * ## Реализовать функцию полного клонирования объекта.
 *
 * ## Технічні вимоги:
 * - Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
 * - Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
 * - В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.
 * 
 */
"use strict";

const person = {
	name: "Jonh",
	surname: "Doe",

	familyMembers: {
		sister: "Clair",
		brother: "Kevin",
		mother: "Joanne",
		father: "David",

		pets: ["Dog", { pet: "Cat" }],

		familyHobbies: {
			sister: ["painting"],
			brother: ["video games", "basketball"],
		}
	},

	languages: ["English", "Spanish", "German"],
}

const copyDeepObject = (objectToClone) => {
	// Если newObject - массив, создается массив, иначе - объект.
	let newObject = Array.isArray(objectToClone) ? [] : {};

	// Если newObject - массив
	if (Array.isArray(newObject)) {
		objectToClone.forEach((arrayValue, index) => {
			// Если в objectToClone находится ещё массив, вызываем рекурсию. Иначе - присваеваем newObject с индексом index значение массива objectToClone с тем же индексом
			(Array.isArray(arrayValue) || (typeof arrayValue === "object")) ? newObject[index] = copyDeepObject(arrayValue): newObject[index] = arrayValue;
		});
	}
	// Проходимся по всем ключам объекта, который клонируем.
	for (let key in objectToClone) {
		newObject[key] = objectToClone[key];

		// Если ключ клонируемого объекта является объектом, вызываем рекурсию.
		if ((typeof objectToClone[key] === "object") && (objectToClone[key] !== null)) {
			// Проходися по вложенным объектам / массивам.
			newObject[key] = copyDeepObject(objectToClone[key]);
		}

	}
	return newObject;
}

const copiedPerson = copyDeepObject(person);

person.name = "Mark";
person.familyMembers.sister = "Klara";
person.familyMembers.pets = ["Shark"];
person.languages = ["Portuguese"];
delete person.familyMembers.familyHobbies.sister;

console.log(person);
console.log(copiedPerson);