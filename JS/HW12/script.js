"use strict";

const btnCollection = document.querySelectorAll('.btn');

document.addEventListener('keydown', (e) => {
	btnCollection.forEach(btn => {
		e.key === btn.dataset.key ? btn.classList.add('btn--active') : btn.classList.remove('btn--active');
	})
})